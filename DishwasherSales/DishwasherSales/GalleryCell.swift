//
//  GalleryCell.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 03/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import UIKit

class GalleryCell: UITableViewCell {
    
    @IBOutlet weak var galleryImageView: UIImageView!
    
    func configure(urlString: String) {
        if let url = NSURL(string: urlString) {
            galleryImageView.sd_setImageWithURL(url, completed: { [weak self] (image, err, cache, url) in
                self?.galleryImageView.image = image
            })
        }
    }
}