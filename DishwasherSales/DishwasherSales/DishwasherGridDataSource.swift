//
//  DishwasherGridDataSource.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 01/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import UIKit

protocol DishwasherGridDataSourceDelegate: class {
    func isFetching() -> Bool
    func DishwasherGridDataSourceDidSelectRow(indexPath: NSIndexPath)
}

class DishwasherGridDataSource: NSObject {
    
    subscript(index: Int) -> Dishwasher? {
        return dishwashers?[index]
    }
    
    var spacing: CGFloat {
        get {
            return 2
        }
    }
    
    var dishwashers: [Dishwasher]?
    
    var count: Int {
        get {
            return dishwashers?.count ?? 0
        }
    }
    
    lazy var spinner: UIActivityIndicatorView = {
        let v = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
        return v
    }()
    
    weak var delegate: DishwasherGridDataSourceDelegate?
    
    init(delegate: DishwasherGridDataSourceDelegate?) {
        self.delegate = delegate
        
        super.init()
    }
    
    /**
     Updates the datasource with new data
     
     - parameter newSource: new data source.
     */
    func updateDataSource(newSource: [Dishwasher]?) {
        dishwashers = newSource
    }
    
    /**
     Defines wether a spinner cell should be shown depending on if the delegate is fetching and no current data is shown
     
     - returns: Boolean value deciding if a spinner should be shown.
     */
    func shouldShowSpinner() -> Bool {
        return (delegate?.isFetching() ?? false) && count == 0
    }
}

extension DishwasherGridDataSource: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if shouldShowSpinner() {
            return 1
        }
        return dishwashers?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if shouldShowSpinner() {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("SpinnerCell", forIndexPath: indexPath)
            cell.contentView.addSubview(spinner)
            spinner.startAnimating()
            spinner.center = cell.center
            return cell
        } else {
            if let cell = collectionView.dequeueReusableCellWithReuseIdentifier(String(DishwasherCollectionViewCell), forIndexPath: indexPath) as? DishwasherCollectionViewCell {
                
                if let data = dishwashers?[indexPath.row] {
                    cell.configure(data.title, price: data.prettyPriceString(), image: data.imageURLString)
                }
                
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
}

extension DishwasherGridDataSource: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        delegate?.DishwasherGridDataSourceDidSelectRow(indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if shouldShowSpinner() {
            return collectionView.frame.size
        } else {
            let width = collectionView.frame.width
            let spacedWidth = width - (spacing * 5)
            let cellWidth = floor(spacedWidth / 4)
            let cellHeight = floor(cellWidth * 1.29)
            return CGSize(width: cellWidth, height: cellHeight)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let theSpacing = shouldShowSpinner() ? 0 : spacing
        return UIEdgeInsets(top: theSpacing, left: theSpacing, bottom: theSpacing, right: theSpacing)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return shouldShowSpinner() ? 0 : spacing

    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return shouldShowSpinner() ? 0 : spacing
    }
}
