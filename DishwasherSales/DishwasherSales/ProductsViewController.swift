//
//  ProductsViewController.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 01/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let c = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        c.translatesAutoresizingMaskIntoConstraints = false
        self.registerCells(c)
        c.dataSource = self.dataSource
        c.delegate = self.dataSource
        c.backgroundColor = UIColor.grayColor()
        return c
    }()
    
    lazy var dataSource: DishwasherGridDataSource = {
        let source = DishwasherGridDataSource(delegate: self)
        return source
    }()
    
    /// Controls whether or not to show the spinner in the collectionview
    var fetching: Bool = false {
        didSet {
            if dataSource.count == 0 {
                collectionView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addCollectionView(collectionView)
    
        updateHeaderText()
        
        fetchDishwashers()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        resetView()
    }
    
    /**
     Fixes a bug where if you go to details and change orientation then return the layout would incorrect.
     */
    func resetView() {
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    /**
     Add collectionview to view.
     
     - parameter collectionView: the collectionview to add.
     */
    func addCollectionView(collectionView: UICollectionView) {
        if collectionView.superview == nil {
            view.addSubview(collectionView)
            NSLayoutConstraint.activateConstraints(ProductsViewController.pinToSuperConstraints(collectionView))
        }
    }
    
    /**
     Constraints for a view to be pinned to all sides of its superview with 0 spacing
     
     - parameter view: the view to be pinned
     
     - returns: returns array of constraints for pinning to super.
     */
    class func pinToSuperConstraints(view: UIView) -> [NSLayoutConstraint] {
        let views = ["a": view]
        var constraints = [NSLayoutConstraint]()
        
        let h = "H:|[a]|"
        let v = "V:|[a]|"
        
        let H_Constraint = NSLayoutConstraint.constraintsWithVisualFormat(h, options: [], metrics: nil, views: views) ?? [NSLayoutConstraint]()
        let V_Constraint = NSLayoutConstraint.constraintsWithVisualFormat(v, options: [], metrics: nil, views: views) ?? [NSLayoutConstraint]()
        
        constraints.appendContentsOf(H_Constraint)
        constraints.appendContentsOf(V_Constraint)
        
        return constraints
    }
    
    /**
     Registers collectionview cells for use
     
     - parameter collectionView: collectionview to register cells to.
     */
    func registerCells(collectionView: UICollectionView) {
        collectionView.registerNib(UINib(nibName: String(DishwasherCollectionViewCell), bundle: nil), forCellWithReuseIdentifier: String(DishwasherCollectionViewCell))
        collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "SpinnerCell")
    }
}

extension ProductsViewController: DishwasherGridDataSourceDelegate {
    
    /**
     Delegate method for returning if the controller is fetching data
     
     - returns: Boolean value on current fetching state.
     */
    func isFetching() -> Bool {
        return fetching
    }
    
    /**
     Delegate method when a row is selected.
     
     - parameter indexPath: indexPath that was pressed.
     */
    func DishwasherGridDataSourceDidSelectRow(indexPath: NSIndexPath) {
        if let washer = dataSource[indexPath.row] {
            presentDetails(washer)
        }
    }
    
    /**
     Presents the product details view controller
     
     - parameter washer: The dishwasher to show details for.
     */
    func presentDetails(washer: Dishwasher) {
        let details = ProductDetailsViewController(dishwasher: washer)
        
        if let nav = navigationController {
            nav.pushViewController(details, animated: true)
        } else {
            presentViewController(details, animated: true, completion: nil)
        }
    }
}

// MARK: - UI
extension ProductsViewController {
    
    /**
     Updates navigation title based on datasource count.
     */
    func updateHeaderText() {
        navigationItem.title = "Dishwashers" + (dataSource.count == 0 ? "" : " (\(dataSource.count))")
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.collectionViewLayout.invalidateLayout()
        
        coordinator.animateAlongsideTransition({ [weak self] (context) in
            self?.collectionView.layoutIfNeeded()
            }) { (context) in
                
        }
        
    }
}

// MARK: - API
extension ProductsViewController {
    
    /**
     Fetches available dishwashers from API
     */
    func fetchDishwashers() {
        fetching = true
        
        DishwasherAPI.requestDishwashers({ [weak self] (json) in
            self?.fetching = false
            
            let washers = ProductsViewController.createDishwashers(json)
            
            self?.updateDataSource(washers)
        }) { [weak self] in
                self?.fetching = false
        }
    }
    
    /**
     Creates dishwashers using dishwasher parser.
     
     - parameter json: JSON representing possible dishwashers
    
     - returns: returns an array of any dishwashers that could be created from provided JSON.
     */
    class func createDishwashers(json: NSDictionary?) -> [Dishwasher]? {
        guard let json = json else {
            return nil
        }
        let washers = DishwasherParser.parseDishwashers(json)
        return washers
    }
    
    /**
     Adds any data provided to the datasource and reloads the collectionview
     
     - parameter washers: Store of potential Dishwashers.
     */
    func updateDataSource(washers: [Dishwasher]?) {
        dataSource.updateDataSource(washers)
        updateHeaderText()
        collectionView.reloadData()
    }
}
