//
//  PriceInformationView.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 03/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import Foundation

class PriceInformationView: UIView {
    
    static let height: CGFloat = 83
    
    static let defaultGaruntee = "Claim an extra 3 years garuntee via redemtion"
    
    static let defaultStandardGaruntee = "2 year garuntee included"
    
    class func instanceFromNib() -> PriceInformationView {
        return UINib(nibName: String(PriceInformationView), bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! PriceInformationView
    }
    
    class func constraints(view: UIView) -> [NSLayoutConstraint] {
        let views = ["a": view]
        var constraints = [NSLayoutConstraint]()
        
        
        let h = "H:|[a]|"
        let v = "V:|[a(==\(PriceInformationView.height))]"
        
        let H_Constraint = NSLayoutConstraint.constraintsWithVisualFormat(h, options: [], metrics: nil, views: views) ?? [NSLayoutConstraint]()
        let V_Constraint = NSLayoutConstraint.constraintsWithVisualFormat(v, options: [], metrics: nil, views: views) ?? [NSLayoutConstraint]()
        
        constraints.appendContentsOf(H_Constraint)
        constraints.appendContentsOf(V_Constraint)
        
        return constraints
    }
    
    @IBOutlet weak var priceLabel: UILabel! {
        didSet {
            priceLabel.font = UIFont.systemFontOfSize(20)
        }
    }
    
    @IBOutlet weak var garunteeInfo: UILabel! {
        didSet {
            garunteeInfo.font = UIFont.systemFontOfSize(14)
            garunteeInfo.textColor = UIColor.redColor()
        }
    }
    
    @IBOutlet weak var standardGaruntee: UILabel! {
        didSet {
            standardGaruntee.font = UIFont.systemFontOfSize(14)
            standardGaruntee.textColor = UIColor.greenColor()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func configure(price: String?, garuntee: String?, standardGarunteeText: String?) {
        priceLabel.text = price
        garunteeInfo.text = garuntee
        standardGaruntee.text = standardGarunteeText
    }
}