//
//  ProductDetailsViewControlelr.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 02/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController {
    
    let washer: Dishwasher
    
    init(dishwasher: Dishwasher) {
        self.washer = dishwasher
        
        super.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        
        // Dependency inection in case other init is used.
        
        let id = "123"
        let title = "Super Washer"
        let nowPrice = "123.45"
        let image = "http://image.com/image.png"
        
        self.washer = Dishwasher(id: id, title: title, nowPrice: nowPrice, imageURLString: image)

        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        // Dependency inection in case other init is used.
        
        let id = "123"
        let title = "Super Washer"
        let nowPrice = "123.45"
        let image = "http://image.com/image.png"
        
        self.washer = Dishwasher(id: id, title: title, nowPrice: nowPrice, imageURLString: image)
        
        super.init(coder: aDecoder)
    }
    
    lazy var manager: ProductDetailsTableViewManager = {
        let v = ProductDetailsTableViewManager(delegate: self)
        return v
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.registerCells(tableView)
        tableView.separatorStyle = .None
        return tableView
    }()
    
    lazy var landscapeDetailsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.whiteColor()
        self.configureAccessoryView(view)
        return view
    }()
    
    func registerCells(tableView: UITableView) {
        tableView.registerNib(UINib(nibName: String(LabelCell), bundle: nil), forCellReuseIdentifier: String(LabelCell))
        tableView.registerNib(UINib(nibName: String(GalleryCell), bundle: nil), forCellReuseIdentifier: String(GalleryCell))
        tableView.registerClass(PriceInfoCell.self, forCellReuseIdentifier: String(PriceInfoCell))
        tableView.registerNib(UINib(nibName: String(ProductSpecCell), bundle: nil), forCellReuseIdentifier: String(ProductSpecCell))
    }
    
    override func viewDidLoad() {
         super.viewDidLoad()
    
        configureViews(tableView, accessoryView: landscapeDetailsView)
        
        automaticallyAdjustsScrollViewInsets = false
        
        tableView.delegate = manager
        tableView.dataSource = manager
        
        tableView.reloadData()
        
        navigationItem.title = washer.title
    }
    
    /**
     Configures the views and constraints them.
     
     - parameter tableView:     the tableview
     - parameter accessoryView: the accessory view, seen when landscape
     */
    func configureViews(tableView: UITableView, accessoryView: UIView) {
        if accessoryView.superview == nil {
            view.addSubview(accessoryView)
        }
        
        if tableView.superview == nil {
            view.addSubview(tableView)
            NSLayoutConstraint.activateConstraints(customTableViewConstraints(tableView, accessoryView: landscapeDetailsView))
        }
    }
    
    /**
     Custom constraints for the view
     
     - parameter tableView:     tableview to be shown
     - parameter accessoryView: accessory view to be shown in landscape
     
     - returns: Array of constraints.
     */
    func customTableViewConstraints(tableView: UITableView, accessoryView: UIView) -> [NSLayoutConstraint] {
        let views = ["a": tableView, "b": accessoryView]
        var constraints = [NSLayoutConstraint]()
        
        let width = min(view.frame.width, view.frame.height)
        
        let h = "H:|[a(==\(width))][b]|"
        let v = "V:|-64-[a]|"
        let accessoryV = "V:|-64-[b]|"
        
        let H_Constraint = NSLayoutConstraint.constraintsWithVisualFormat(h, options: [], metrics: nil, views: views) ?? [NSLayoutConstraint]()
        let V_Constraint = NSLayoutConstraint.constraintsWithVisualFormat(v, options: [], metrics: nil, views: views) ?? [NSLayoutConstraint]()
        let Accessory_V_Constraint = NSLayoutConstraint.constraintsWithVisualFormat(accessoryV, options: [], metrics: nil, views: views) ?? [NSLayoutConstraint]()
        
        constraints.appendContentsOf(H_Constraint)
        constraints.appendContentsOf(V_Constraint)
        constraints.appendContentsOf(Accessory_V_Constraint)
        
        return constraints
    }

    /**
     Checks the views height and width to decide wether the view is in landscape
     
     - returns: Boolean value based on wether the views width is greater than its height.
     */
    func isLandscape() -> Bool {
        return view.frame.width > view.frame.height
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animateAlongsideTransition({ (c) in
            
            }) { [weak self] (c) in
                self?.tableView.reloadData()
        }
        
    }
    
    func configureAccessoryView(view: UIView) {
        let price = PriceInformationView.instanceFromNib()
        view.addSubview(price)
        NSLayoutConstraint.activateConstraints(PriceInformationView.constraints(price))
        price.configure(washer.prettyPriceString(), garuntee: PriceInformationView.defaultGaruntee, standardGarunteeText: PriceInformationView.defaultStandardGaruntee)
    }
}

extension ProductDetailsViewController: ProductDetailsTableViewManagerDelegate {
    
    func ProductDetailsTableViewManagerShouldDisplayCompact() -> Bool {
        return isLandscape()
    }
    
    func ProductDetailsTableViewManagerImageURLString() -> String? {
        return washer.imageURLString
    }
    
    func ProductDetailsTableViewManagerPriceInfo() -> String? {
        return washer.prettyPriceString()
    }
}
