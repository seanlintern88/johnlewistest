//
//  DishwasherParser.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 01/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import Foundation

class DishwasherParser {
    
    /**
     Creates an array of dishwasher objects
     
     - parameter json: JSON data representing the potential dishwashers
     
     - returns: returns optional array of dishwashers
     */
    class func parseDishwashers(json: NSDictionary) -> [Dishwasher]? {
        if let washersArray = json["products"] as? [NSDictionary] {
            var dishwashers = [Dishwasher]()
            for washer in washersArray {
                let id = washer["productId"] as? String
                let title = washer["title"] as? String
                var image = washer["image"] as? String
                
                if let i = image {
                    let cleanedImage = i.stringByReplacingOccurrencesOfString("//", withString: "http://")
                    image = cleanedImage
                }
                
                var price = "0"
                
                if let priceObj = washer["price"] as? NSDictionary {
                    if let now = priceObj["now"] as? String {
                        price = now
                    }
                }
                
                if let id = id, title = title, image = image {
                    let dishwasher = Dishwasher(id: id, title: title, nowPrice: price, imageURLString: image)
                    dishwashers.append(dishwasher)
                }
            }
            
            return dishwashers
        }
        return nil
    }
    
}