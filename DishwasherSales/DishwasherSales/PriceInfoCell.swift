//
//  PriceInfoCell.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 03/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import Foundation

class PriceInfoCell: UITableViewCell {
    
    lazy var priceInfo: PriceInformationView = {
        let v = PriceInformationView.instanceFromNib()
        return v
    }()
    
    func configure(price: String?, garuntee: String?, standardGarunteeText: String?) {
        if priceInfo.superview == nil {
            contentView.addSubview(priceInfo)
            NSLayoutConstraint.activateConstraints(PriceInformationView.constraints(priceInfo))
        }
        
        priceInfo.configure(price, garuntee: garuntee, standardGarunteeText: standardGarunteeText)
    }
}