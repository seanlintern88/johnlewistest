//
//  DishwasherCollectionViewCell.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 02/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import UIKit

class DishwasherCollectionViewCell: UICollectionViewCell {
    
    class func instanceFromNib() -> DishwasherCollectionViewCell {
        return UINib(nibName: String(DishwasherCollectionViewCell), bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! DishwasherCollectionViewCell
    }
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.font = UIFont.systemFontOfSize(14)
            titleLabel.textColor = UIColor.grayColor()
        }
    }
    
    @IBOutlet weak var priceLabel: UILabel! {
        didSet {
            priceLabel.font = UIFont.systemFontOfSize(16)
            priceLabel.textColor = UIColor.blackColor()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        // This prevents old images being reused and showing before new image is loaded in
        imageView.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.backgroundColor = UIColor.whiteColor()
    }
    
    /**
     Configures the cell for display.
     
     - parameter title: Text for the label directly under the image
     - parameter price: Text for the bottom price label
     - parameter image: String URL for the image to be used in the imageview
     */
    func configure(title: String, price: String, image: String) {
        titleLabel.text = title
        priceLabel.text = price

        updateImage(image, imageView: imageView)
    }
    
    func updateImage(image: String, imageView: UIImageView) {
        if let url = NSURL(string: image) {
            imageView.sd_setImageWithURL(url, completed: { (image, err, cache, theURL) in
                imageView.image = image
            })
        }
    }
}