//
//  ProductSpecCell.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 03/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import Foundation

class ProductSpecCell: UITableViewCell {
    
    @IBOutlet weak var topLabel: UILabel! {
        didSet {
            topLabel.textColor = UIColor.grayColor()
        }
    }
    
    @IBOutlet weak var middleLabel: UILabel! {
        didSet {
            middleLabel.textColor = UIColor.grayColor()
        }
    }
    
    @IBOutlet weak var bottomLabel: UILabel! {
        didSet {
            bottomLabel.textColor = UIColor.grayColor()
        }
    }
    
    func configure(topText: String?, middleText: String?, bottomText: String?) {
        topLabel.text = topText
        middleLabel.text = middleText
        bottomLabel.text = bottomText
    }
}
