//
//  ProductDetailsTableViewManager.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 02/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import Foundation

protocol ProductDetailsTableViewManagerDelegate: class {
    func ProductDetailsTableViewManagerShouldDisplayCompact() -> Bool
    func ProductDetailsTableViewManagerImageURLString() -> String?
    func ProductDetailsTableViewManagerPriceInfo() -> String?
}

class ProductDetailsTableViewManager: NSObject {
    
    weak var delegate: ProductDetailsTableViewManagerDelegate?
    
    init(delegate: ProductDetailsTableViewManagerDelegate) {
        self.delegate = delegate
    
        super.init()
    }
}

extension ProductDetailsTableViewManager: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
}

extension ProductDetailsTableViewManager: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0: // Image gallery
            return min(tableView.frame.width, tableView.frame.height)
        case 1: // Regular: Price info, Compact: Product info
            if delegate?.ProductDetailsTableViewManagerShouldDisplayCompact() ?? false {
                return UITableViewAutomaticDimension
            } else {
                return PriceInformationView.height + 10
            }
        case 2: // Regular: Product info, Compact: Read More
            if delegate?.ProductDetailsTableViewManagerShouldDisplayCompact() ?? false {
                let values = valuesForIndexPath(indexPath)
                return values.height
            } else {
                return UITableViewAutomaticDimension
            }
        case 3...10: // Label Cell
            let values = valuesForIndexPath(indexPath)
            return values.height
        default:
            return 50
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0: // Image gallery
            if let cell = tableView.dequeueReusableCellWithIdentifier(String(GalleryCell), forIndexPath: indexPath) as? GalleryCell {
                if let urlString = delegate?.ProductDetailsTableViewManagerImageURLString() {
                    cell.configure(urlString)
                }
                return cell
            }
        case 1: // Regular: Price info, Compact: Product info
            if delegate?.ProductDetailsTableViewManagerShouldDisplayCompact() ?? false {
                if let cell = tableView.dequeueReusableCellWithIdentifier(String(ProductSpecCell), forIndexPath: indexPath) as? ProductSpecCell {
                    cell.configure("Product Information", middleText: "Product Code : sf807f0s97f90s", bottomText: "This is some \n\n really long text")
                    return cell
                }
            } else {
                if let cell = tableView.dequeueReusableCellWithIdentifier(String(PriceInfoCell), forIndexPath: indexPath) as? PriceInfoCell {
                    cell.configure(delegate?.ProductDetailsTableViewManagerPriceInfo(), garuntee: PriceInformationView.defaultGaruntee, standardGarunteeText: PriceInformationView.defaultStandardGaruntee)
                    return cell
                }
            }
        case 2: // Regular: Product info, Compact: Read More
            if delegate?.ProductDetailsTableViewManagerShouldDisplayCompact() ?? false {
                if let cell = tableView.dequeueReusableCellWithIdentifier(String(LabelCell), forIndexPath: indexPath) as? LabelCell {
                    configureCell(cell, indexPath: indexPath)
                    cell.leftLabel.textColor = UIColor.blackColor()
                    return cell
                }
            } else {
                if let cell = tableView.dequeueReusableCellWithIdentifier(String(ProductSpecCell), forIndexPath: indexPath) as? ProductSpecCell {
                    cell.configure("Product Information", middleText: "This is some \n\n really long text", bottomText: "Product Code : sf807f0s97f90s")
                    return cell
                }
            }
        case 3...10: // Label Cell
            if let cell = tableView.dequeueReusableCellWithIdentifier(String(LabelCell), forIndexPath: indexPath) as? LabelCell {
                configureCell(cell, indexPath: indexPath)
                return cell
            }
        default:
            return UITableViewCell()
        }
        
        return UITableViewCell()
    }
}

// MARK: - Cells
extension ProductDetailsTableViewManager {
    
    func configureCell(cell: LabelCell, indexPath: NSIndexPath) {
        let values = valuesForIndexPath(indexPath)
        cell.configure(values.leftText, rightText: values.rightText, font: values.font)
    }
    
    /**
     Configuration values for the basic label cells
     
     - parameter indexPath: indexpath of cell to be configured
     
     - returns: values for the provided indexpath.
     */
    func valuesForIndexPath(indexPath: NSIndexPath) -> (leftText: String, rightText: String?, font: UIFont, height: CGFloat) {
        switch indexPath.row {
        case 2:
            return ("Read More", nil, UIFont.systemFontOfSize(16), 50)
        case 3:
            return ("Product Specification", nil, UIFont.systemFontOfSize(20), 100)
        case 4:
            return ("Adjustable Racking Information", "Yes", UIFont.systemFontOfSize(16), 50)
        case 5:
            return ("Child lock Door & control lock Delay Start", "Yes", UIFont.systemFontOfSize(16), 50)
        case 6:
            return ("Delay Wash", "Yes", UIFont.systemFontOfSize(16), 50)
        case 7:
            return ("Delicate Wash", "Yes", UIFont.systemFontOfSize(16), 50)
        case 8:
            return ("Dimensions", "H85 x W60 x D60", UIFont.systemFontOfSize(16), 50)
        case 9:
            return ("Drying Performance", "A", UIFont.systemFontOfSize(16), 50)
        case 10:
            return ("Drying System", "Heat Exchanger", UIFont.systemFontOfSize(16), 50)

        default:
            return ("undefined", "undefined", UIFont.systemFontOfSize(12), 50)
        }
    }
}
