//
//  LabelCell.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 03/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import Foundation

class LabelCell: UITableViewCell {
    
    @IBOutlet weak var leftLabel: UILabel! {
        didSet {
            leftLabel.textColor = UIColor.grayColor()
        }
    }
    
    @IBOutlet weak var rightLabel: UILabel! {
        didSet {
            rightLabel.textColor = UIColor.grayColor()
        }
    }
    
    func configure(leftText: String?, rightText: String?, font: UIFont) {
        leftLabel.text = leftText
        rightLabel.text = rightText
        leftLabel.font = font
        rightLabel.font = font
        
        leftLabel.textColor = UIColor.grayColor()
        rightLabel.textColor = UIColor.grayColor()
    }
}
