//
//  DishwasherAPI.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 01/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import Foundation

typealias DishwasherAPISuccess = (json: NSDictionary) -> ()
typealias DishwasherAPIFailure = () -> Void
typealias NSURLDataTaskCompletionHandler = (NSData?, NSURLResponse?, NSError?) -> Void

class DishwasherAPI {
    
    // Provided URL string
    static let API_URL: String = "https://api.johnlewis.com/v1/products/search?q=dishwasher&key=Wu1Xqn3vNrd1p7hqkvB6hEu0G9OrsYGb&pageSize=20"
    
    /**
     Builds NSURLRequest for given URL string.
     
     - parameter urlString: String representing url for the request.
     
     - returns: produces optional get request if provided URL string is valid.
     */
    class func getRequest(urlString: String) -> NSURLRequest? {
        guard let url = NSURL(string: urlString) else {
            return nil
        }
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        return request
    }
    
    /**
     Private implementation of the dishawasher API request.
     
     - parameter completionHandler: Contains the response information from the get request.
     */
    class func requestDishwashersAPI(completionHandler: NSURLDataTaskCompletionHandler?) {
        guard let request = DishwasherAPI.getRequest(DishwasherAPI.API_URL) else {
            return
        }
        
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            if let completionHandler = completionHandler {
                completionHandler(data, response, error)
            }
        }.resume()
    }
    
    /**
     Calls dishwasher API for a page of dishwashers.
     
     - parameter success:                Optional return for success, contains dishwasher details.
     - parameter failure:                Optional return for failure.
     */
    class func requestDishwashers(success: DishwasherAPISuccess?, failure: DishwasherAPIFailure?) {
        DishwasherAPI.requestDishwashersAPI { (data, response, error) in
            dispatch_async(dispatch_get_main_queue(),{
                if let data = data {
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data, options: [])
                        
                        if let success = success, let json = json as? NSDictionary {
                            success(json: json)
                        }
                    } catch {
                        if let failure = failure {
                            failure()
                        }
                    }
                } else {
                    if let failure = failure {
                        failure()
                    }
                }
            })
        }
    }
}
