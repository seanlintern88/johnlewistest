//
//  Dishwasher.swift
//  DishwasherSales
//
//  Created by Sean Lintern on 01/08/2016.
//  Copyright © 2016 Sean Lintern. All rights reserved.
//

import Foundation

/**
 *  Dishwasher struct, contains details about a dishwasher.
 */
struct Dishwasher {
    let id: String
    let title: String
    let nowPrice: String
    let imageURLString: String
    
    func prettyPriceString() -> String {
        return "£\(nowPrice)"
    }
}