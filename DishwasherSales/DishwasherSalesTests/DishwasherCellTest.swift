import XCTest
@testable import DishwasherSales

class DishwasherCellTest: XCTestCase {
    
    func testConfigure() {
        let title = "Super Washer"
        let nowPrice = "123.45"
        let image = "http://image.com/image.png"
        
        let cell = DishwasherCollectionViewCell.instanceFromNib()
        
        cell.configure(title, price: nowPrice, image: image)
    
        XCTAssertEqual(cell.titleLabel.text, title)
        XCTAssertEqual(cell.priceLabel.text, nowPrice)
    }
    
}