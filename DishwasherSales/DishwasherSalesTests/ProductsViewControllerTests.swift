import XCTest
@testable import DishwasherSales

class ProductsViewControllerTests: XCTestCase {

    func testCreateWashers() {
        let result = ProductsViewController.createDishwashers(nil)
       
        XCTAssertNil(result)
    }

    func testUpdatingDataSource() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(String(ProductsViewController)) as! ProductsViewController
        let washer = Dishwasher(id: "123", title: "Washer", nowPrice: "124.45", imageURLString: "imageURL")
        let washers = [washer]
        
        vc.updateDataSource(washers)
        
        XCTAssertEqual(washers.count, vc.dataSource.count)
    }
}
