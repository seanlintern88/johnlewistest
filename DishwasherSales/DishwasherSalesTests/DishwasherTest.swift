import XCTest
@testable import DishwasherSales

class DishwasherTest: XCTestCase {

    func testDishwasherAttributes() {
        let id = "123"
        let title = "Super Washer"
        let nowPrice = "123.45"
        let image = "http://image.com/image.png"
        
        let washer = Dishwasher(id: id, title: title, nowPrice: nowPrice, imageURLString: image)

        XCTAssertEqual(washer.id, id)
        XCTAssertEqual(washer.title, title)
        XCTAssertEqual(washer.nowPrice, nowPrice)
        XCTAssertEqual(washer.imageURLString, image)
    }
    
    func testPriceDisplayString() {
        let id = "123"
        let title = "Super Washer"
        let nowPrice = "123.45"
        let image = "http://image.com/image.png"
        
        let washer = Dishwasher(id: id, title: title, nowPrice: nowPrice, imageURLString: image)

        let testString = "£" + "\(nowPrice)"
        
        XCTAssertEqual(testString, washer.prettyPriceString())
    }
    
}