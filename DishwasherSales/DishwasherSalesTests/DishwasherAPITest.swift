import XCTest
@testable import DishwasherSales

class DishwasherAPITest: XCTestCase {
    
    func testValidRequest() {
        let potentialRequest = DishwasherAPI.getRequest(DishwasherAPI.API_URL)
        
        XCTAssertNotNil(potentialRequest)
    }
}