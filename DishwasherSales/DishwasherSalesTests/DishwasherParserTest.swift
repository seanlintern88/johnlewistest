import XCTest
@testable import DishwasherSales

class DishwasherParserTest: XCTestCase {
    
    func testParseDishwashersNil() {
        let json = ["": ""]
        
        let result = DishwasherParser.parseDishwashers(json)
    
        XCTAssertNil(result)
    }
    
    func testParseDishwashers() {
        let json = ["products": [["productId": "123", "title": "Dishwasher Test", "image": "http://Image.jpg", "price": ["now": "123.45"]]]]
    
        let result = DishwasherParser.parseDishwashers(json)
        
        XCTAssertNotNil(result)
    }
}