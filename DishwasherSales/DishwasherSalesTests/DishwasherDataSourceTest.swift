import XCTest
@testable import DishwasherSales

class DishwasherDataSourceTest: XCTestCase {
 
    func testUpdateDatasource() {
        let washer = Dishwasher(id: "123", title: "Washer", nowPrice: "124.45", imageURLString: "imageURL")
        let washers = [washer]

        let source = DishwasherGridDataSource(delegate: nil)
        
        source.updateDataSource(washers)
        
        XCTAssertEqual(washers.count, source.count)
    }
 
    func testShouldShowSpinnerFalse() {
        let source = DishwasherGridDataSource(delegate: nil)

        let result = source.shouldShowSpinner()
        
        XCTAssertFalse(result)
    }
    
    func testShouldShowSpinnerTrue() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(String(ProductsViewController)) as! ProductsViewController
        vc.fetching = true

        let source = DishwasherGridDataSource(delegate: vc)
        
        let result = source.shouldShowSpinner()

        XCTAssertTrue(result)
    }
    
    func testNumberOfSections() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(String(ProductsViewController)) as! ProductsViewController

        let source = DishwasherGridDataSource(delegate: nil)
        
        let count = source.numberOfSectionsInCollectionView(vc.collectionView)
        
        XCTAssertEqual(count, 1)
    }
    
    func testNumberOfRows() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(String(ProductsViewController)) as! ProductsViewController

        let washer = Dishwasher(id: "123", title: "Washer", nowPrice: "124.45", imageURLString: "imageURL")
        let washers = [washer]

        let source = DishwasherGridDataSource(delegate: nil)
        
        source.updateDataSource(washers)
        
        let count = source.collectionView(vc.collectionView, numberOfItemsInSection: 0)
        
        XCTAssertEqual(washers.count, count)
    }
    
    func testNumberOfRowsNoWashers() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(String(ProductsViewController)) as! ProductsViewController
        
        let source = DishwasherGridDataSource(delegate: nil)
                
        let count = source.collectionView(vc.collectionView, numberOfItemsInSection: 0)
        
        XCTAssertEqual(0, count)
    }
    
    func testNumberOfRowsWithSpinner() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(String(ProductsViewController)) as! ProductsViewController
        vc.fetching = true
        let source = DishwasherGridDataSource(delegate: vc)
        
        let count = source.collectionView(vc.collectionView, numberOfItemsInSection: 0)
        
        XCTAssertEqual(1, count)
    }
    
    func testSubscript() {
        let washer = Dishwasher(id: "123", title: "Washer", nowPrice: "124.45", imageURLString: "imageURL")
        let washers = [washer]
        
        let source = DishwasherGridDataSource(delegate: nil)
        
        source.updateDataSource(washers)

        let potentialWasher = source[0]
        
        XCTAssertEqual(potentialWasher?.title, washer.title)
    }
}